require 'rest-client'
require 'nokogiri'
require 'json'
require 'addressable/uri'

origin_uri = Addressable::URI.new(
  :scheme => "http",
  :host => "maps.googleapis.com",
  :path => "maps/api/geocode/json",
  :query_values => {
    address: "770 Broadway St, New York, NY",
    sensor: true}
).to_s

origin = JSON.parse(RestClient.get(origin_uri))

lat = origin["results"].first["geometry"]["location"]["lat"]
lng = origin["results"].first["geometry"]["location"]["lng"]

results_uri = Addressable::URI.new(
  :scheme => "https",
  :host => "maps.googleapis.com",
  :path => "maps/api/place/nearbysearch/json",
  :query_values => {
    location: "#{lat},#{lng}",
    radius: 10,
    keyword: "ice cream",
    sensor: false,
    key: "AIzaSyBUBJWYjz713UE_asg7NhGcsI8qktU9TXo"}
).to_s

results =  JSON.parse(RestClient.get(results_uri))

options = {}

puts "Select Store:"
i = 1
results["results"].each do |result|
  puts "#{i} #{result["name"]}"
  options[i] = result["name"]
  i += 1
end

input = Integer(gets.chomp)
name_of_the_store = options[input]

destination = nil
results["results"].each do |result|
  if result.values.include?(name_of_the_store)
    destination = "#{result["geometry"]["location"]["lat"]},#{result["geometry"]["location"]["lng"]}"
    break
  end
end

directions_uri = Addressable::URI.new(
  :scheme => "http",
  :host => "maps.googleapis.com",
  :path => "maps/api/directions/json",
  :query_values => {
    origin: "#{lat},#{lng}",
    destination: "#{destination}",
    sensor: false,
    mode: "walking"}
).to_s

directions = JSON.parse(RestClient.get(directions_uri))

directions["routes"].first["legs"].first["steps"].each do |key, val|
  parsed_html = Nokogiri::HTML(key["html_instructions"])
  puts parsed_html.text
end